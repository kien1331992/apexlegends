package com.kien.apexapp.util;

public class Const {
    public static final String EXITS_DB="isDb";

    public static final String KEY_INSTALL_APP="isInstall";
    public static final String KEY_INTENT_INSTALL_APP="intent_map";
    public static final String KEY_INTENT_HERO="intent_hero";
    public static final String KEY_INTENT_WEAPON="intent_WEAPON";
    public static final String KEY_INTENT_ABILITY_PASSIVE="intent_ability_passive";
    public static final String KEY_INTENT_ABILITY_ULTIMATE="intent_ability_ultimate";
    public static final String KEY_INTENT_TYPE_ABILITY="intent_type_ability";
}
