package com.kien.apexapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kien.apexapp.Model.Map;
import com.kien.apexapp.R;
import com.kien.apexapp.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class MapAdapter extends RecyclerView.Adapter<MapAdapter
        .DetailsViewHolder> {
    List<Map> lvMap;
    Context context;
    LayoutInflater inflater;
    private HeroAdapter.ItemClickListener mClickListener;

    public MapAdapter(Context ct, List<Map> _lvMap) {
        this.context = ct;
        this.lvMap = _lvMap;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public MapAdapter.DetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_map, parent, false);
        return new MapAdapter.DetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MapAdapter.DetailsViewHolder holder, int position) {
        Map map = lvMap.get(position);
        holder.tvTitle.setText(map.getTitle().trim());
        holder.tvDescription.setText(map.getDescription().trim());

        ArrayList<String> nodes = StringUtil.formatChildMap("-",map.getNode());
        String fnode = "";
        for (String node : nodes) {
            fnode += "- ";
            fnode += node.trim()+"\n";
        }
        holder.tvNode.setText(fnode);

    }


    @Override
    public int getItemCount() {
        return lvMap.size();
    }

    public class DetailsViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvDescription;
        TextView tvNode;

        public DetailsViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvNode = itemView.findViewById(R.id.tvNode);

        }

    }

}
