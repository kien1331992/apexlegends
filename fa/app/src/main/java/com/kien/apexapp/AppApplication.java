package com.kien.apexapp;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class AppApplication extends Application {
    private static AppApplication mSelf;

    public static AppApplication self() {
        return mSelf;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mSelf = this;
    }
}
