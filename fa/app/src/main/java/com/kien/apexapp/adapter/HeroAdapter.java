package com.kien.apexapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kien.apexapp.R;
import com.kien.apexapp.util.ImageUtils;

import java.util.List;

public class HeroAdapter extends RecyclerView.Adapter<HeroAdapter
        .DetailsViewHolder> {
    List<String> lvHero;
    Context context;
    LayoutInflater inflater;
    private ItemClickListener mClickListener;

    public HeroAdapter(Context ct, List<String> _lvHero) {
        this.context = ct;
        this.lvHero = _lvHero;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public DetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_hero, parent, false);
        return new DetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailsViewHolder holder, int position) {
        Glide.with(this.context).load(ImageUtils.getImageDrawble(this.context,lvHero.get(position))).into(holder.imv_hero);
    }

    @Override
    public int getItemCount() {
        return lvHero.size();
    }

    public class DetailsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imv_hero;

        public DetailsViewHolder(View itemView) {
            super(itemView);
            imv_hero = itemView.findViewById(R.id.imvHero);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);
    }
}
