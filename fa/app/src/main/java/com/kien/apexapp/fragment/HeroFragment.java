package com.kien.apexapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kien.apexapp.Model.Hero;
import com.kien.apexapp.R;
import com.kien.apexapp.activity.DetailHeroActivity;
import com.kien.apexapp.adapter.HeroAdapter;
import com.kien.apexapp.util.Const;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HeroFragment extends Fragment implements HeroAdapter.ItemClickListener {
    String[] typedArray;
    ArrayList<String> strUriCard;

    @BindView(R.id.rvHero)
    RecyclerView rvHero;

    private HeroAdapter heroAdapter;
    private ArrayList<Hero> heros;

    public HeroFragment() {
        // Required empty public constructor
    }

    public static HeroFragment newInstance(ArrayList<Hero> _hero) {
        // Required empty public constructor
        HeroFragment heroFragment = new HeroFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Const.KEY_INTENT_HERO, _hero);
        heroFragment.setArguments(bundle);
        return heroFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hero, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        typedArray = getResources().getStringArray(R.array.arr_card_hero);
        if (typedArray.length > 0)
            strUriCard = new ArrayList<String>(Arrays.asList(typedArray));
        heroAdapter = new HeroAdapter(getActivity(), strUriCard);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvHero.hasFixedSize();
        rvHero.setLayoutManager(linearLayoutManager);
        rvHero.setAdapter(heroAdapter);
        heroAdapter.setClickListener(this);

        Bundle bundle = getArguments();
        if (bundle != null)
            heros = (ArrayList<Hero>) bundle.getSerializable(Const.KEY_INTENT_HERO);
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), DetailHeroActivity.class);
        intent.putExtra(Const.KEY_INTENT_HERO, heros.get(position));
        startActivity(intent);
    }
}
