package com.kien.apexapp.Model;

import com.kien.apexapp.Database.annotation.PrimaryKey;

import java.io.Serializable;

public class Ability implements Serializable {
    @PrimaryKey
    private int id;

    private String name;
    private String type;
    private String description;
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
