package com.kien.apexapp.Model;

import com.kien.apexapp.Database.annotation.PrimaryKey;

import java.io.Serializable;

public class Weapon implements Serializable{
    @PrimaryKey
    private int id;

    private String name;
    private String image;
    private String dame_body;
    private String head_shot;
    private String attach;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDame_body() {
        return dame_body;
    }

    public void setDame_body(String dame_body) {
        this.dame_body = dame_body;
    }

    public String getHead_shot() {
        return head_shot;
    }

    public void setHead_shot(String head_shot) {
        this.head_shot = head_shot;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
