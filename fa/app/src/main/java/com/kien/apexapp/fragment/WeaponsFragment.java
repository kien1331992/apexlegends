package com.kien.apexapp.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kien.apexapp.Model.Weapon;
import com.kien.apexapp.R;
import com.kien.apexapp.activity.WeaponDetailActivity;
import com.kien.apexapp.adapter.HeroAdapter;
import com.kien.apexapp.util.Const;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeaponsFragment extends Fragment implements HeroAdapter.ItemClickListener {
    @BindView(R.id.rvWeapon)
    RecyclerView rvWeapon;

    private HeroAdapter heroAdapter;
    private ArrayList<String> weaponsUri=new ArrayList<>();
    private ArrayList<Weapon> arrWeaponds;

    public WeaponsFragment() {
        // Required empty public constructor
    }

    public static WeaponsFragment newInstance(ArrayList<Weapon> weapons) {
        WeaponsFragment fragment = new WeaponsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Const.KEY_INTENT_WEAPON, weapons);
        fragment.setArguments(bundle);
        // Required empty public constructor
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weapons, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            arrWeaponds = (ArrayList<Weapon>) bundle.getSerializable(Const.KEY_INTENT_WEAPON);
            for (Weapon weapon : arrWeaponds) {
                weaponsUri.add(weapon.getImage());
            }
        }


        heroAdapter = new HeroAdapter(getActivity(), weaponsUri);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvWeapon.hasFixedSize();
        rvWeapon.setLayoutManager(linearLayoutManager);
        rvWeapon.setAdapter(heroAdapter);
        heroAdapter.setClickListener(this);
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(getActivity(), WeaponDetailActivity.class);
        intent.putExtra(Const.KEY_INTENT_WEAPON, arrWeaponds.get(position));
        startActivity(intent);
    }
}
