package com.kien.apexapp.Model;

import com.kien.apexapp.Database.annotation.PrimaryKey;

import java.io.Serializable;

public class TypeAbility implements Serializable {

    @PrimaryKey
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
