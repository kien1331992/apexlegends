package com.kien.apexapp.Model;

import com.kien.apexapp.Database.annotation.PrimaryKey;

import java.io.Serializable;

public class Map implements Serializable{
    @PrimaryKey
    private int id;

    private String title;
    private String description;
    private String node;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }
}
