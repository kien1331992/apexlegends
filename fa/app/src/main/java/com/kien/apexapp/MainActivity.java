package com.kien.apexapp;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.kien.apexapp.Database.SQLiteDAO;
import com.kien.apexapp.Model.Ability;
import com.kien.apexapp.Model.Hero;
import com.kien.apexapp.Model.Map;
import com.kien.apexapp.Model.TypeAbility;
import com.kien.apexapp.Model.Weapon;
import com.kien.apexapp.fragment.HeroFragment;
import com.kien.apexapp.fragment.HomeFragment;
import com.kien.apexapp.fragment.WeaponsFragment;
import com.kien.apexapp.util.Const;
import com.kien.apexapp.util.SharedPrefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    SharedPrefs sp;
    @BindView(R.id.frm)
    FrameLayout frm;
    @BindView(R.id.adView)
    AdView adView;
    private BottomNavigationView navigation;

    private ArrayList<Hero> dsHero;
    private ArrayList<Ability> dsAbility;
    private ArrayList<Map> dsMap;
    private ArrayList<TypeAbility> dsTypeAbility;
    private ArrayList<Weapon> dsWeapond;
    private InterstitialAd mInterstitialAd;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setFragmentView(HomeFragment.newInstance(dsMap));
                    showAds();
                    return true;
                case R.id.navigation_dashboard:
                    showAds();
                    setFragmentView(HeroFragment.newInstance(dsHero));
                    return true;
                case R.id.navigation_notifications:
                    showAds();
                    setFragmentView(WeaponsFragment.newInstance(dsWeapond));
                    return true;
            }
            return false;
        }
    };

    private void showAds() {
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, "ca-app-pub-8687783974050352~1078918190");

        ButterKnife.bind(this);
        navigation = (BottomNavigationView) findViewById(R.id
                .navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        sp = SharedPrefs.getInstance();
        checkPermission();
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8687783974050352/5986919506");


        final RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) navigation
                .getLayoutParams();
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int error) {
                adView.setVisibility(View.GONE);
                lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                navigation.setLayoutParams(lp);
            }

            @Override
            public void onAdLoaded() {
                adView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setFragmentView(Fragment fm) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frm, fm, "fragmentTag");
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            Dexter.withActivity(MainActivity.this)
                    .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                // do you work now
                                checkInstallDb();
                            } else {
                                finish();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest>
                                                                               permissions,
                                                                       PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .check();
        } else {
            checkInstallDb();
        }
    }

    private void checkInstallDb() {
        String username = sp.get(Const.KEY_INSTALL_APP, null);
        if (username == null) {
            SharedPrefs.getInstance().put(Const.KEY_INSTALL_APP, 1);
            createDb();
        }
        loadDb();
    }

    //kiem tra xem db co ton tai hay khong va tao moi no
    private void createDb() {
        SQLiteDAO.initialize(this);
        if (!SQLiteDAO.checkDB()) {
            SQLiteDAO.createDB();
        }
    }

    private void loadDb() {
        dsHero = SQLiteDAO.createQuery(Hero.class).perform();
        dsAbility = SQLiteDAO.createQuery(Ability.class).perform();
        dsMap = SQLiteDAO.createQuery(Map.class).perform();
        dsTypeAbility = SQLiteDAO.createQuery(TypeAbility.class).perform();
        dsWeapond = SQLiteDAO.createQuery(Weapon.class).perform();

        setFragmentView(HomeFragment.newInstance(dsMap));
    }

    @Override
    public void onBackPressed() {
        showAds();
        navigation.setSelectedItemId( R.id.navigation_home);
    };
}
