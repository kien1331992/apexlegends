package com.kien.apexapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kien.apexapp.Model.Ability;
import com.kien.apexapp.Model.Hero;
import com.kien.apexapp.R;
import com.kien.apexapp.util.Const;
import com.kien.apexapp.util.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailHeroActivity extends AppCompatActivity {
    @BindView(R.id.imvHero)
    ImageView imvHero;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvSubName)
    TextView tvSubName;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvFullName)
    TextView tvFullName;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.tvTactics)
    TextView tvTactics;
    @BindView(R.id.tvPassive)
    TextView tvPassive;
    @BindView(R.id.tvUltilmate)
    TextView tvUltilmate;
    @BindView(R.id.tvNameTacticsAblitity)
    TextView tvNameTacticsAblitity;
    @BindView(R.id.tvNamePassive)
    TextView tvNamePassive;
    @BindView(R.id.tvNameUltilmate)
    TextView tvNameUltilmate;
    @BindView(R.id.tvContenTatics)
    TextView tvContenTatics;
    @BindView(R.id.tvContentPassive)
    TextView tvContentPassive;
    @BindView(R.id.tvContentUltimate)
    TextView tvContentUltimate;

    private Hero hero;
    private Ability abTatics;
    private Ability abPassive;
    private Ability abUltimate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_hero);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            hero = (Hero) bundle.getSerializable(Const.KEY_INTENT_HERO);
            abTatics = hero.getAbilityTatics();
            abPassive = hero.getAbilityPassive();
            abUltimate = hero.getAbility();

            fillData();
        }

    }

    private void fillData() {
        Glide.with(this).load(ImageUtils.getImageDrawble(this, hero.getUrl())).into(imvHero);
        tvName.setText(hero.getName());
        tvSubName.setText(hero.getSub_name());
        tvDescription.setText(hero.getDescription());
        tvAge.setText(hero.getAge() == 0 ? "None" : hero.getAge() + "");
        tvFullName.setText(hero.getReal_name());

        tvTactics.setText(abTatics.getName());
        tvPassive.setText(abPassive.getName());
        tvUltilmate.setText(abUltimate.getName());

        tvNameTacticsAblitity.setText(abTatics.getName());
        tvNameTacticsAblitity.setText(abPassive.getName());
        tvNameTacticsAblitity.setText(abUltimate.getName());

        tvContenTatics.setText(abTatics.getDescription());
        tvContentPassive.setText(abPassive.getDescription());
        tvContentPassive.setText(abUltimate.getDescription());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
