package com.kien.apexapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kien.apexapp.Model.Weapon;
import com.kien.apexapp.R;
import com.kien.apexapp.util.Const;
import com.kien.apexapp.util.ImageUtils;
import com.kien.apexapp.util.StringUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeaponDetailActivity extends AppCompatActivity {
    @BindView(R.id.imvHeader)
    ImageView imvHeader;
    @BindView(R.id.tvHeadShot)
    TextView tvHeadShot;
    @BindView(R.id.tvBodyShot)
    TextView tvBodyShot;
    @BindView(R.id.tvAttachment)
    TextView tvAttachment;
    @BindView(R.id.tvDescription)
    TextView tvDescription;

    private Weapon weapon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weapon_detail);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            weapon = (Weapon) bundle.getSerializable(Const.KEY_INTENT_WEAPON);
            fillData();
        }
    }

    private void fillData() {
        Glide.with(this).load(ImageUtils.getImageDrawble(this, weapon.getImage())).into(imvHeader);
        tvHeadShot.setText("Head shot: " + weapon.getHead_shot());
        tvBodyShot.setText("Body shot: " + weapon.getDame_body());
        tvDescription.setText(weapon.getDescription());

        ArrayList<String> nodes = StringUtil.formatChildMap("/", weapon.getAttach());
        String fnode = "";
        for (String node : nodes) {
            fnode += "- ";
            fnode += node.trim() + "\n";
        }
        tvAttachment.setText(fnode);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
