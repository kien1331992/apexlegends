package com.kien.apexapp.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kien.apexapp.Model.Map;
import com.kien.apexapp.R;
import com.kien.apexapp.adapter.MapAdapter;
import com.kien.apexapp.util.Const;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    @BindView(R.id.rvMap)
    RecyclerView rvMap;

    ArrayList<Map> arrMapInfor;
    private String strValueMap;
    private MapAdapter mapAdapter;


    public static HomeFragment newInstance(ArrayList<Map> _arrMapInfor) {
        HomeFragment myFragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putSerializable(Const.KEY_INTENT_INSTALL_APP, _arrMapInfor);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        Bundle bund = getArguments();
        if (bund != null) {
            arrMapInfor = (ArrayList<Map>) bund.getSerializable(Const.KEY_INTENT_INSTALL_APP);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mapAdapter = new MapAdapter(getActivity(), arrMapInfor);
        rvMap.hasFixedSize();
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rvMap.setLayoutManager(manager);
        rvMap.setAdapter(mapAdapter);
    }
}
