package com.kien.apexapp.util;

import android.content.Context;

public class ImageUtils {
    public static int getImageDrawble(Context context, String imageName) {
        imageName = StringUtil.cutExtendImage(imageName);
        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable",
                context.getPackageName());

        return drawableResourceId;
    }
}
