package com.kien.apexapp.Model;

import com.kien.apexapp.Database.annotation.OneToOne;
import com.kien.apexapp.Database.annotation.PrimaryKey;

import java.io.Serializable;

public class Hero implements Serializable{
    @PrimaryKey
    private int id;

    private String name;
    private String url;
    private String description;
    private String sub_name;
    private String real_name;
    private int age;
    private int tatics;
    private int passive;
    private int ultimate;


    @OneToOne(joinProperty = "tatics",typeOfJoin = Ability.class)
    Ability abilityTatics;
    @OneToOne(joinProperty = "passive",typeOfJoin = Ability.class)
    Ability abilityPassive;
    @OneToOne(joinProperty = "ultimate",typeOfJoin = Ability.class)
    Ability abilityUltilmate;

    public Ability getAbilityTatics() {
        return abilityTatics;
    }

    public void setAbilityTatics(Ability abilityTatics) {
        this.abilityTatics = abilityTatics;
    }

    public Ability getAbilityPassive() {
        return abilityPassive;
    }

    public void setAbilityPassive(Ability abilityPassive) {
        this.abilityPassive = abilityPassive;
    }

    public Ability getAbility() {
        return abilityUltilmate;
    }

    public void setAbility(Ability ability) {
        this.abilityUltilmate = ability;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTatics() {
        return tatics;
    }

    public void setTatics(int tatics) {
        this.tatics = tatics;
    }

    public int getPassive() {
        return passive;
    }

    public void setPassive(int passive) {
        this.passive = passive;
    }

    public int getUltimate() {
        return ultimate;
    }

    public void setUltimate(int ultimate) {
        this.ultimate = ultimate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
